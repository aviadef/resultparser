import os
import argparse
import csv



##### DEFINES #######
LOGPATH = r"/home/adsl/aviad/gnuradio/epfl_result/epflresult.txt"
PATHCSV  = r"/home/adsl/aviad/gnuradio/epfl_result/epflresult.csv"

###### presets ######
RXFIELDS = ['Payload_length','CRC_presence','Coding_rate','Header_checksum','payload','CRC','Frame_number']
HEADERLOGFIEDLS = ['Payload length','CRC presence','Coding rate','Header checksum','msg']
LOGTYPE = 2 # 1: RX+TX, 2:RX, 3:Tx 



def parse(data, PATHCSV,LOGTYPE):
    hitcnt = 0
    flag = 0
    if LOGTYPE == 2:
        # genrate CSV Header
        with open(PATHCSV, mode='w') as csvfile:
             csvwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
             # writing the fields
             csvwriter.writerow(RXFIELDS)
            
             for line in data: 
                if (type(line) == str) & ('--header--'.upper() in line.upper()):
                    # Recieve section
                    flag  = 1
                    rx_values = []
                
                if (('CRC invalid'.upper() in line.upper()) |
                    ('CRC valid!'.upper() in line.upper())):
                    # end of paragraph
                    flag = 0

                    rx_values.append(line.strip()) 

                    # writing the fields
                    csvwriter.writerow(rx_values)
                
                #res = any(item in data_string for item in listA)
                if (flag==1) and ( any(item in line for item in HEADERLOGFIEDLS)):
                    rx_values.append(line.strip()) 
    
    else: 
        pass


def main(LOGPATH, PATHCSV):

    with open(LOGPATH,"r") as inputfile:
        data = inputfile.readlines()
    
    parse(data, PATHCSV, LOGTYPE)

if __name__ == "__main__":  
    main(LOGPATH,PATHCSV)